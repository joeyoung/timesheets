{
    "AWSTemplateFormatVersion": "2010-09-09",
    "Description": "AWS Cloud Formation template for the RDS Timesheets Database.",
    "Parameters": {
        "Vpc": {
            "Type": "String",
            "Description": "VpcId of your existing Virtual Private Cloud (VPC)"
        },
        "SubnetGroup": {
            "Type": "String",
            "Description" : "DB Subnet Group Id to put the instance in"
        },
        "SecurityGroup": {
            "Type": "String",
            "Description": "VPC Security Group Id to put the RDS instance in"
        },
        "MultiAZ": {
            "Type": "String",
            "Description": "Run the instance in MultiAZ, true or false",
            "AllowedValues" : ["true", "false"],
            "Default": "false",
            "ConstraintDescription" : "Must be true or false"
        },
        "DbName": {
            "Default": "timesheets",
            "Description": "The database name",
            "Type": "String",
            "MinLength": "1",
            "MaxLength": "64",
            "AllowedPattern":"[a-zA-Z][a-zA-Z0-9]*",
            "ConstraintDescription": "must begin with a letter and contain only alphanumeric characters"
        },
        "DbUsername": {
            "Default": "admin",
            "NoEcho": "true",
            "Description": "The database admin account username",
            "Type": "String",
            "MinLength": "1",
            "MaxLength": "16",
            "AllowedPattern": "[a-zA-Z][a-zA-Z0-9]*",
            "ConstraintDescription": "must begin with a letter and contain only alphanumeric characters."
        },
        "DbPassword": {
            "Default": "password",
            "NoEcho": "true",
            "Description" : "The database admin account password",
            "Type": "String",
            "MinLength": "8",
            "MaxLength": "41",
            "AllowedPattern" : "[a-zA-Z0-9]*",
            "ConstraintDescription" : "must contain only alphanumeric characters."
        },
        "DbClass" : {
            "Default" : "db.t1.micro",
            "Description" : "Database instance class",
            "Type" : "String",
            "AllowedValues" : [ "db.t1.micro", "db.m1.small", "db.m1.large", "db.m1.xlarge", "db.m2.xlarge", "db.m2.2xlarge", "db.m2.4xlarge" ],
            "ConstraintDescription" : "must select a valid database instance type."
        },
        "DbAllocatedStorage" : {
            "Default": "5",
            "Description" : "The size of the database (Gb)",
            "Type": "Number",
            "MinValue": "5",
            "MaxValue": "1024",
            "ConstraintDescription" : "must be between 5 and 1024Gb."
        }
    },
    "Resources": {
        "Database" : {
            "Type" : "AWS::RDS::DBInstance",
            "Properties" : {
                "DBName" : { "Ref" : "DbName" },
                "AllocatedStorage" : { "Ref" : "DbAllocatedStorage" },
                "DBInstanceClass" : { "Ref" : "DbClass" },
                "Engine" : "MySQL",
                "EngineVersion" : "5.6",
                "MasterUsername" : { "Ref" : "DbUsername" } ,
                "MasterUserPassword" : { "Ref" : "DbPassword" },
                "DBSubnetGroupName" : { "Ref" : "SubnetGroup" },
                "VPCSecurityGroups" : [ { "Ref" : "SecurityGroup" }],
                "MultiAZ": {"Ref" : "MultiAZ"},
                "Tags": [{"Key": "Name", "Value" : "Timesheets"}]
            }
        }
    },
    "Outputs": {
        "JDBCConnectionString": {
            "Description" : "JDBC connection string for database",
            "Value" : { "Fn::Join": [ "", [ "jdbc:mysql://",
                { "Fn::GetAtt": [ "Database", "Endpoint.Address" ] },
                ":",
                { "Fn::GetAtt": [ "Database", "Endpoint.Port" ] },
                "/",
                { "Ref": "DbName" }]]}
        }
    }
}