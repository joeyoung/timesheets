package org.timesheets.services.impl;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.timesheets.data.Query;
import org.timesheets.data.QueryContext;
import org.timesheets.domain.employee.EmployeeEntity;
import org.timesheets.domain.employee.EmployeeRepository;
import org.timesheets.services.EmployeeService;

import java.util.List;

/**
 * Created By: jyoung
 * Date: 11/4/13
 * Time: 7:57 AM
 */

@Service
@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository repository;
    private QueryContext queryContext;

    @Autowired
    public EmployeeServiceImpl(QueryContext queryContext, EmployeeRepository repository) {
        this.queryContext = queryContext;
        this.repository = repository;
    }

    @Override
    public List<EmployeeEntity> getAll() {
        List<EmployeeEntity> employees = queryContext.execute(new EmployeeQuery());

        return employees;
    }


    @Override
    public EmployeeEntity create(String name, String department) {
        EmployeeEntity employee = new EmployeeEntity(name, department);

        repository.add(employee);

        return employee;
    }

    public class EmployeeQuery implements Query<List<EmployeeEntity>> {

        @Override
        @SuppressWarnings("unchecked")
        public List<EmployeeEntity> execute(Session session) {

            return session.createQuery("from EmployeeEntity e order by e.name")
                    .list();
        }
    }
}
