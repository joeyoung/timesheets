package org.timesheets.services;

import org.timesheets.domain.employee.EmployeeEntity;

import java.util.List;

/**
 * Created By: jyoung
 * Date: 11/4/13
 * Time: 7:57 AM
 */
public interface EmployeeService {
    List<EmployeeEntity> getAll();
    EmployeeEntity create(String name, String department);
}
