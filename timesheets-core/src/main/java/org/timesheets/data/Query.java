package org.timesheets.data;

import org.hibernate.Session;

/**
 * Created By: jyoung
 * Date: 11/4/13
 * Time: 8:47 AM
 */
public interface Query<TResult> {
    TResult execute(Session session);
}
