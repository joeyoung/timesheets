package org.timesheets.data;

/**
 * Created By: jyoung
 * Date: 11/6/13
 * Time: 7:53 AM
 */
public interface QueryContext {
    <TResult> TResult execute(Query<TResult> query);
}
