package org.timesheets.data.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.timesheets.domain.DomainRepository;
import org.timesheets.domain.AggregateRoot;
import org.timesheets.domain.DomainEntity;

import java.lang.reflect.ParameterizedType;

/**
 * Created By: jyoung
 * Date: 10/28/13
 * Time: 9:06 AM
 */
@Repository("hibernateRepository")
public class HibernateRepository<TRoot extends DomainEntity & AggregateRoot> implements DomainRepository<TRoot> {

    private SessionFactory sessionFactory;

    @Autowired
    public final void setSessionFactory(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    protected Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public void add(TRoot entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void remove(TRoot entity) {
        getCurrentSession().delete(entity);
    }

    @Override
    @SuppressWarnings("unchecked")
    public TRoot get(Integer id) {

        Class<? extends TRoot> doaType = (Class<TRoot>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];

        return (TRoot)getCurrentSession().get(doaType, id);
    }
}
