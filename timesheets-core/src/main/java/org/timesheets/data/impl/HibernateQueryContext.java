package org.timesheets.data.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.timesheets.data.Query;
import org.timesheets.data.QueryContext;

/**
 * Created By: jyoung
 * Date: 11/6/13
 * Time: 7:55 AM
 */
@Service("queryContext")
public class HibernateQueryContext implements QueryContext {

    private SessionFactory sessionFactory;

    @Autowired
    public final void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public <TResult> TResult execute(Query<TResult> query) {
        return query.execute(sessionFactory.getCurrentSession());
    }
}
