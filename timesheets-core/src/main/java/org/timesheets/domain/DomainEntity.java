package org.timesheets.domain;

import javax.persistence.*;

/**
 * Created By: Joe
 * Date: 10/23/13
 * Time: 10:33 PM
 */
@MappedSuperclass
public class DomainEntity {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    public Integer getId() {
        return id;
    }
}
