package org.timesheets.domain.employee.impl;

import org.springframework.stereotype.Repository;
import org.timesheets.data.impl.HibernateRepository;
import org.timesheets.domain.employee.EmployeeEntity;
import org.timesheets.domain.employee.EmployeeRepository;

/**
 * Created By: jyoung
 * Date: 11/4/13
 * Time: 3:34 PM
 */

@Repository("employeeRepository")
public class EmployeeRepositoryImpl extends HibernateRepository<EmployeeEntity> implements EmployeeRepository {
    // no custom implementation
}
