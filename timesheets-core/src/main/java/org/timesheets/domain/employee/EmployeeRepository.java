package org.timesheets.domain.employee;

import org.timesheets.domain.DomainRepository;

/**
 * Created By: Joe
 * Date: 10/23/13
 * Time: 10:29 PM
 */
public interface EmployeeRepository extends DomainRepository<EmployeeEntity> {
    // no custom implementation
}
