package org.timesheets.domain.employee;

import org.timesheets.domain.AggregateRoot;
import org.timesheets.domain.DomainEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * Created By: Joe
 * Date: 10/23/13
 * Time: 10:24 PM
 */
@Entity
@Table(name = "employee")
public class EmployeeEntity extends DomainEntity implements AggregateRoot {

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String department;

    protected EmployeeEntity() {}

    public EmployeeEntity(String name, String department) {
        this.name = name;
        this.department = department;
    }

    public String getName() {
        return name;
    }

    public String getDepartment() {
        return department;
    }
}
