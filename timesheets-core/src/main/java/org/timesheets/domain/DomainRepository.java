package org.timesheets.domain;

import org.timesheets.domain.AggregateRoot;
import org.timesheets.domain.DomainEntity;

/**
 * Created By: Joe
 * Date: 10/23/13
 * Time: 10:28 PM
 */
public interface DomainRepository<TEntity extends DomainEntity & AggregateRoot> {
    void add(TEntity entity);
    void remove(TEntity entity);
    TEntity get(Integer id);
}
