package org.timesheets.data;

/**
 * Created By: jyoung
 * Date: 11/6/13
 * Time: 9:58 AM
 */
public class Sql {

    public static class  Count {
        public static class All {
            public static String employees() {
                return "select count(*) from employee";
            }
        }
    }

    public static class Insert {
        public static class One {
            public static String employee() {
                return "insert into employee (name, department) values (?, ?)";
            }
        }
    }

    public static class Delete {
        public static class All {
            public static String[] data() {
                    return new String[] {"delete from employee"};
            }
        }
    }
}
