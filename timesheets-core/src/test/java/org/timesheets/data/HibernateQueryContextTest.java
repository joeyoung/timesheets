package org.timesheets.data;

import org.hibernate.Session;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.timesheets.data.impl.HibernateQueryContext;
import org.timesheets.domain.employee.EmployeeEntity;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created By: jyoung
 * Date: 11/6/13
 * Time: 9:59 AM
 */
public class HibernateQueryContextTest extends DataTest {

    @Autowired
    private HibernateQueryContext queryContext;

    @Test
    @Transactional
    public void canExecuteQuery() {

        // arrange
        database.update(Sql.Insert.One.employee(), new Object[]{"a", "b"});

        // act
        List<EmployeeEntity> employees = queryContext.execute(new TestQuery());

        // assert
        assertEquals(1, employees.size());
    }

    public class TestQuery implements Query<List<EmployeeEntity>>{

        @SuppressWarnings("unchecked")
        @Override
        public List<EmployeeEntity> execute(Session session) {
            return session.createQuery("from EmployeeEntity e order by e.name")
                    .list();
        }
    }
}
