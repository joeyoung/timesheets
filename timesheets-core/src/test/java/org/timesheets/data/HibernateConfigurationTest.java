package org.timesheets.data;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import static junit.framework.Assert.assertNotNull;

/**
 * Created By: jyoung
 * Date: 10/28/13
 * Time: 9:45 AM
 */
@ContextConfiguration(locations = "/applicationContext-Core.xml")
public class HibernateConfigurationTest extends AbstractJUnit4SpringContextTests {

    @Autowired
    private SessionFactory sessionFactory;

    @Test
    public void testConfigurationIsValid() {
        assertNotNull(sessionFactory);
    }
}
