package org.timesheets.data;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import org.timesheets.data.impl.HibernateRepository;
import org.timesheets.domain.employee.EmployeeEntity;

import static junit.framework.Assert.assertTrue;

/**
 * Created By: jyoung
 * Date: 11/6/13
 * Time: 11:09 AM
 */
public class HibernateRepositoryTest extends DataTest {

    @Autowired
    @Qualifier("hibernateRepository")
    HibernateRepository<EmployeeEntity> repository;

    @Test
    @Transactional
    public void canAddEntity() {

        // arrange
        EmployeeEntity employee = new EmployeeEntity("a", "b");

        // act
        repository.add(employee);

        // assert
        Integer count  = database.queryForObject(Sql.Count.All.employees(), Integer.class);

        assertTrue(count == 1);
    }
}
