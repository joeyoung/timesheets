package org.timesheets.data;

import org.hibernate.SessionFactory;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

/**
 * Created By: jyoung
 * Date: 11/6/13
 * Time: 9:52 AM
 */
@ContextConfiguration(locations = {"/applicationContext-Core.xml"})
public abstract class DataTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    protected JdbcTemplate database;

    @Autowired
    protected SessionFactory sessionFactory;

    @Before
    public void deleteAllData() {
        database.batchUpdate(Sql.Delete.All.data());
    }
}
