package org.timesheets.domain;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.timesheets.domain.employee.EmployeeEntity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


/**
 * Created By: jyoung
 * Date: 11/19/13
 * Time: 12:41 PM
 */
@RunWith(JUnit4.class)
public class EmployeeEntityTests {

    @Test
    public void canCreateEmployee() {

        // arrange

        // act
        EmployeeEntity employee = new EmployeeEntity("a", "b");

        // assert
        assertNotNull(employee);
        assertEquals(employee.getName(), "a");
        assertEquals(employee.getDepartment(), "b");
    }
}
