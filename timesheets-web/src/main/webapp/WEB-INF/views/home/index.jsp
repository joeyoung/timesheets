<%@ page contentType='text/html;charset=UTF-8' language='java' %>
<%@ taglib prefix='fmt' uri='http://java.sun.com/jsp/jstl/fmt' %>
<%@ taglib prefix='spring' uri='http://www.springframework.org/tags' %>

<html>
    <head>
        <title>Welcome to Timesheet app!</title>
    </head>
    <body>
        <h1>Welcome to the Timesheet App!</h1>
        <ul>
            <%--<li><a href='#'>List managers</a></li>--%>
            <li><a href='/api/employees'>List employees</a></li>
            <%--<li><a href='#'>List tasks</a></li>--%>
            <%--<li><a href='#'>List timesheets</a></li>--%>
        </ul>
        Today is: <fmt:formatDate value='${today}' pattern='dd-MM-yyyy'/>
        <p>Environment: ${environment}</p>
    </body>
</html>