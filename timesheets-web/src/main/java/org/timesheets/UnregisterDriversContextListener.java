
package org.timesheets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created By: jyoung
 * Date: 11/8/13
 * Time: 9:21 AM
 */

/**
 * Deregisters the MySql driver to preven tomcat from freaking out about it
 */

public class UnregisterDriversContextListener implements ServletContextListener {

    final static Logger log = LoggerFactory.getLogger(UnregisterDriversContextListener.class);
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

        log.info("Shutting Down Context");


        try {
            log.info("Deregistring MySqlDriver");
            Driver mySqlDriver = DriverManager.getDriver("jdbc:mysql://localhost:3306/timesheet");
            DriverManager.deregisterDriver(mySqlDriver);
        } catch (SQLException ex) {
            log.info("Could not deregister driver:".concat(ex.getMessage()));
        }
    }
}
