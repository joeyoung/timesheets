package org.timesheets.web.models;

/**
 * Created By: jyoung
 * Date: 10/31/13
 * Time: 3:48 PM
 */
public class Error {

    private String field;
    private String message;

    public Error(String field, String message) {
        this.field = field;
        this.message = message;
    }

    public String getField() {
        return field;
    }

    public String getMessage() {
        return message;
    }
}
