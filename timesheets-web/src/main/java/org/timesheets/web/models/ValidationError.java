package org.timesheets.web.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created By: jyoung
 * Date: 10/31/13
 * Time: 3:47 PM
 */
public class ValidationError {

    private List<Error> errors = new ArrayList<>();

    public void addError(String field, String message) {
        errors.add(new Error(field, message));
    }

    public List<Error> getErrors() {
        return errors;
    }
}
