package org.timesheets.web.controllers.api.employee;

import org.hibernate.validator.constraints.NotBlank;

/**
 * Created By: jyoung
 * Date: 10/31/13
 * Time: 7:55 AM
 */
public class Employee {

    public Integer id;

    @NotBlank
    public String name;
    @NotBlank
    public String department;

    public Employee() {
    }

    public Employee(Integer id, String name, String department) {
        this.id = id;
        this.name = name;
        this.department = department;
    }
}
