package org.timesheets.web.controllers.api.employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.timesheets.domain.employee.EmployeeEntity;
import org.timesheets.services.EmployeeService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Created By: jyoung
 * Date: 10/31/13
 * Time: 7:51 AM
 */
@Controller
@RequestMapping("/api/employees")
public class EmployeeController {

    private EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody List<Employee> get() {

        List<Employee> employees = new ArrayList<>();

        List<EmployeeEntity> entities = employeeService.getAll();

        for(EmployeeEntity employee : entities) {
            employees.add(new Employee(employee.getId(), employee.getName(), employee.getDepartment()));
        }

        return employees;
    }

    @RequestMapping(value = "/{id}",  method = RequestMethod.GET)
    public @ResponseBody Employee get(@PathVariable Integer id) {

        return new Employee(1, "Test", "IT");
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<String> create(@RequestBody @Valid Employee model) {

        EmployeeEntity employee = employeeService.create(model.name, model.department);

        HttpHeaders headers = new HttpHeaders();
        headers.set("content-location", "/api/employees/" + employee.getId());

        return new ResponseEntity<>("", headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<String> update(@PathVariable Integer id, @RequestBody Employee employee) {

        HttpHeaders headers = new HttpHeaders();
        headers.set("content-location", "/api/employees/" + id);

        return new ResponseEntity<>("", headers, HttpStatus.OK);
    }
}


