package org.timesheets.web.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Date;

/**
 * Created By: jyoung
 * Date: 10/30/13
 * Time: 4:16 PM
 */
@Controller
@RequestMapping("/")
public class HomeController {

    final static Logger logger = LoggerFactory.getLogger(HomeController.class);

    @Value("${environment}")
    private String environment;


    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {

        logger.info("Home Requested");

//        String environment = System.getProperty("environment");

        model.addAttribute("today", new Date());
        model.addAttribute("environment", environment);

        return "home/index";
    }
}
