Timesheets
=========================

A reworking of the Spring tutorial found at [Java Code Geeks](http://www.javacodegeeks.com/2012/09/spring-designing-domain-model-and.html).

Use Cases
-------------------------
* A Task is created by a Manager
* A Task is assigned to an Employee by a Manager
* An Employee records the hours worked on a Task by creating a Timesheet
* A Manager can alter the hours on a Timesheet that an Employee worked on for a specific Task
* Both Manager and Employees can view 
	- the busiest tasks (Tasks with the most Employees)
	- All Tasks for an individual Employee
	- All Tasks that a Manager has created


Notes
--------------------------
User Data for EC2:

#!/bin/bash -ex
exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1
echo BEGIN USERDATA
sudo service tomcat7 stop
sudo rm -rf ROOT
sudo aws s3 cp s3://20131112-joeyoung-webapps/timesheets/timesheets-web.war /var/lib/tomcat7/webapps/ROOT.war --region us-east-1
sudo service tomcat7 start
echo END USERDATA
