require 'aws-sdk'

CURRENT_DIRECTORY = File.dirname(__FILE__)

# this could be handled by an IAM role if the script was running inside an EC2 instance
AWS.config(
    :access_key_id => 'AKIAJJMQVOVLW3QOP7LQ',
    :secret_access_key => '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'
)

namespace :deploy do

  bucket = '20131112-joeyoung-webapps/timesheets'

  desc 'Create the web and worker deployment package with the production profile'
  task :pack do
    sh 'mvn clean package -P production'
  end

  namespace :web do

    desc 'Copies the web deployment package to the appropriate S3 bucket'
    task :to_s3 => 'deploy:pack' do

      file = "#{CURRENT_DIRECTORY}/timesheets-web/target/timesheets-web.war"
      key = File.basename(file)

      puts "uploading #{key} to s3"

      # get an instance of the s3 interface
      s3 = AWS::S3.new

      # get the file
      s3.buckets[bucket].objects[key].write(:file => file)

      puts "uploaded #{key} to s3"
    end
  end
end

namespace :db do

    MYSQL = 'mysql --login-path=local -e'

    namespace :dev do

      desc 'Create the timesheets database on localhost'
      task :create do
        database_sql = 'create database if not exists `timesheet`'
        user_sql = "grant create, alter, index, select, update, insert, delete on timesheet.* TO 'timesheets'@'localhost' identified by 'password'"

        sh "#{MYSQL} \"#{database_sql}\""
        sh "#{MYSQL} \"#{user_sql}\""

      end

      desc 'Drops the database timesheets database on localhost'
      task :drop do
        database_sql = 'drop database if exists `timesheet`'
        user_sql = "drop user 'timesheets'@'localhost'"

        sh "#{MYSQL} \"#{database_sql}\""
        sh "#{MYSQL} \"#{user_sql}\""
      end

      desc 'Migrates the develoment database'
      task :migrate do
        migrate('development')
      end

      desc 'Rollsback the develoment database by 1'
      task :rollback do
        rollback('development')
      end

      desc 'Get the status of the develoment database'
      task :status do
        status('development')
      end
    end

    namespace :staging do
      desc 'Migrates the staging database'
      task :migrate do
        migrate('staging')
      end

      desc 'Rollsback the staging database by 1'
      task :rollback do
        rollback('staging')
      end

      desc 'Get the status of the staging database'
      task :status do
        status('staging')
      end
    end


    namespace :test do
      desc 'Migrates the test database'
      task :migrate do
        migrate('test')
      end

      desc 'Rollsback the test database by 1'
      task :rollback do
        rollback('test')
      end

      desc 'Get the status of the test database'
      task :status do
        status('test')
      end
    end


    namespace :production do
      desc 'Migrates the production database'
      task :migrate do
        migrate('production')
      end

      desc 'Rollsback the production database by 1'
      task :rollback do
        rollback('production')
      end

      desc 'Get the status of the production database'
      task :status do
        status('production')
      end
    end

    #-----------------------------------------------------
    # database functions
    #-----------------------------------------------------
    def migrate(profile)
      Dir.chdir('timesheets-core') do
        sh "mvn liquibase:update -P #{profile}"
      end
    end

    def rollback(profile)
      Dir.chdir('timesheets-core') do
        sh "mvn liquibase:rollback -Dliquibase.rollbackCount=1 -P #{profile}"
      end
    end

    def status(profile)
      Dir.chdir('timesheets-core') do
        sh "mvn liquibase:status -P #{profile}"
      end
    end

end